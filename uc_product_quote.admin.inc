<?php

/**
 * @file
 * Product Quote administration menu items.
 */

/**
 * Form to associate product quotes with products or classes.
 *
 * @ingroup forms
 * @see
 *   uc_product_quote_form_submit()
 */
function uc_product_quote_form($form_state, $object, $type) {
  switch ($type) {
    case 'class':
      $class = $object;
      $table = '{uc_class_quotes}';
      $primary_key = 'pcid';
      $primary_key_type = "'%s'";
      $id = $class->pcid;
      if (empty($class->name)) {
        drupal_goto('admin/store/products/classes/'. $id);
      }
      drupal_set_title(check_plain($class->name));
      break;
    case 'product':
    default:
      $product = $object;
      $table = '{uc_product_quotes}';
      $primary_key = 'nid';
      $primary_key_type = '%d';
      $id = $product->nid;
      if (empty($product->title)) {
        drupal_goto('node/'. $id);
      }
      drupal_set_title(check_plain($product->title));
  }

  // Retrieve modules that impliment shipping methods.
  $methods = module_invoke_all('shipping_method');
  foreach ($methods as $mid => $method) {
    // If the method is enabled
    if ($method['enabled']) {
      // Keep array of enabled method id's
      $mids[] = $mid;

      // Container for each methods services
      $form[$mid] = array(
        '#type'         => 'fieldset',
        '#title'        => $method['title'],
        '#collapsible'  => TRUE,
        '#collapsed'    => TRUE,
      );

      // Retrieve an array of services already selected for this method
      $services = unserialize(db_result(db_query("SELECT services FROM $table WHERE $primary_key = $primary_key_type AND method = '%s'", $id, $mid)));

      // If no services have previously been selected
      if (!$services) {
        $services = array();
      }

      // List of methods services to restrict for the product
      $form[$mid][$mid] = array(
        '#type'          => 'checkboxes',
        '#title'         => t('Services'),
        '#default_value' => $services,
        '#options'       => $method['quote']['accessorials'],
      );
    }
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );
  $form['mids'] = array(
    '#type' => 'value',
    '#value' => $mids,
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );

  return $form;
}

/**
 * Submit handler for uc_product_quote_form().
 */
function uc_product_quote_form_submit($form, &$form_state) {
  if ($form_state['values']['type'] == 'product') {
    $table = '{uc_product_quotes}';
    $primary_key = 'nid';
    $primary_key_type = '%d';
  }
  elseif ($form_state['values']['type'] == 'class') {
    $table = '{uc_class_quotes}';
    $primary_key = 'pcid';
    $primary_key_type = "'%s'";
  }

  // Loop through form values
  foreach ($form_state['values']['mids'] as $method) {
    // Filter out any unselected services
    $services = array_filter($form_state['values'][$method]);

    // If services were selected
    if (!empty($services)) {
      $serialized_services = serialize($services);
    
      // Try to update an existing record
      db_query("UPDATE $table SET services = '%s' WHERE $primary_key = $primary_key_type and method = '%s'", $serialized_services, $form_state['values']['id'], $method);
    
      // If the update failed
      if (!db_affected_rows()) {
        // Create a new record
        @db_query("INSERT INTO $table ($primary_key, method, services) VALUES ($primary_key_type, '%s', '%s')", $form_state['values']['id'], $method, $serialized_services);
      }
    }
    // If no services were selected for this product
    else {
      // Delete the existing record
      db_query("DELETE FROM $table WHERE $primary_key = $primary_key_type AND method = '%s'", $form_state['values']['id'], $method);
    }
  }

  if ($form_state['values']['type'] == 'product') {
    // Clear all cached product quotes
    cache_clear_all('product_quotes', 'cache');
  }
}
