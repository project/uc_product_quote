<?php

/**
 * @file
 * Product Quote module.
 * 
 * Allows a store administrator to specify which shipping methods/services to
 * enable on a per product and product class basis.
 */

/******************************************************************************
 * Drupal Hooks                                                               *
 ******************************************************************************/

/**
 * Implementation of hook_help().
 */
function uc_product_quote_help($path, $arg) {
  switch ($path) {
    // Help message for selecting product specific shipping quotes.
    case 'node/%/edit/quotes':
      return t('Select the services available for this product. If no services are selected, the default services will be used.');
    case 'admin/store/products/classes/%/quotes':
      return t('Select the services available for this product class. If no services are selected, the default services will be used.');
  }
}

/**
 * Implementation of hook_perm().
 */
function uc_product_quote_perm() {
  return array('administer product quotes');
}

/**
 * Implementation of hook_menu().
 */
function uc_product_quote_menu() {
  // Menu items for default product class quotes.
  $items['admin/store/products/classes/%uc_product_class/quotes'] = array(
    'title' => 'Shipping Quotes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_product_quote_form', 4, 'class'),
    'access callback' => 'uc_product_quote_class_access',
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'uc_product_quote.admin.inc',
  );

  // Insert subitems into the edit node page for product types.
  $items['node/%node/edit/quotes'] = array(
    'title' => 'Shipping Quotes',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uc_product_quote_form', 1, 'product'),
    'access callback' => 'uc_product_quote_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
    'file' => 'uc_product_quote.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_nodeapi().
 */
function uc_product_quote_nodeapi(&$node, $op, $arg3 = NULL, $arg4 = NULL) {
  if (uc_product_is_product($node->type)) {
    switch ($op) {
      case 'insert':
        switch ($GLOBALS['db_type']) {
          case 'mysqli':
          case 'mysql':
            db_query("INSERT IGNORE INTO {uc_product_quotes} (nid, method, services) SELECT %d, method, services FROM {uc_class_quotes} WHERE pcid = '%s'", $node->nid, $node->type);
            break;
          case 'pgsql':
            db_query("INSERT INTO {uc_product_quotes} (nid, method, services) SELECT %d, method, services FROM {uc_class_quotes} WHERE pcid = '%s'", $node->nid, $node->type);
            break;
        }
        break;
      case 'delete':
        db_query("DELETE FROM {uc_product_quotes} WHERE nid = %d", $node->nid);
        break;
    }
  }
}

/******************************************************************************
 * Ubercart Hooks                                                             *
 ******************************************************************************/

/**
 * Implementation of hook_product_class().
 */
function uc_product_quote_product_class($type, $op) {
  switch ($op) {
    case 'delete':
      db_query("DELETE FROM {uc_class_quotes} WHERE pcid = '%s'", $type);
      break;
  }
}

/******************************************************************************
 * FedEx Hooks                                                                *
 ******************************************************************************/

/**
 * Return an array containing only the services allowed for the particular
 */
function uc_product_quote_fedex_filter($products, $details) {
  // Loop through the products
  foreach ($products as $product) {
    $args[] = $product->nid;
  }

  // Filter out duplicate products
  $args = array_unique($args);

  // Build conditions based on unique products
  foreach ($args as $arg) {
    $conditions .= 'nid = %d OR ';
  }
  // Truncate last 'OR'
  $conditions = substr($conditions, 0, strlen($conditions) - 4);

  // Get a list of the default allowed FedEx services
  $services = array_filter(variable_get('uc_fedex_services', _uc_fedex_services()));

  // Get a list of allowed services for each product
  $result = db_query("SELECT services FROM {uc_product_quotes} WHERE ($conditions) AND method = 'fedex'", $args);
  while ($product_services = unserialize(db_result($result))) {
    $services = array_intersect($services, $product_services);
  }

  return array('uc_product_quote' => $services);
}

/******************************************************************************
 * Module Functions                                                           *
 ******************************************************************************/

/**
 * Determine if user has access to product quotes.
 */
function uc_product_quote_access($node) {
  if ($node->type == 'product_kit') {
    return FALSE;
  }

  return uc_product_is_product($node) && node_access('update', $node) && user_access('configure quotes') && user_access('administer product quotes');
}

/**
 * Determine if user has access to product class quotes.
 */
function uc_product_quote_class_access() {
  return user_access('administer product classes') && user_access('administer product quotes');
}
